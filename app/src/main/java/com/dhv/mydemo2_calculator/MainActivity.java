package com.dhv.mydemo2_calculator;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {
    Button modulo,CE,C,backspace,onebyx,xsquare,divide,multiply,minus,plus,equal,seven,eight,nine,four,five,six,three,two,one,signchange,zero,dot;
    TextView result=null;
    double val1,val2;
    boolean add, sub, div, mul,mod,obx,sqx;
    boolean equalcheck = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        one = (Button) findViewById(R.id.btn_1);
        two = (Button) findViewById(R.id.btn_2);
        three = (Button) findViewById(R.id.btn_3);
        four = (Button) findViewById(R.id.btn_4);
        five = (Button) findViewById(R.id.btn_5);
        six = (Button) findViewById(R.id.btn_6);
        seven = (Button) findViewById(R.id.btn_7);
        eight = (Button) findViewById(R.id.btn_8);
        nine = (Button) findViewById(R.id.btn_9);
        zero = (Button) findViewById(R.id.btn_0);
        plus = (Button) findViewById(R.id.btn_plus);
        minus = (Button) findViewById(R.id.btn_minus);
        multiply = (Button) findViewById(R.id.btn_multiply);
        divide = (Button) findViewById(R.id.btn_divide);
        equal = (Button) findViewById(R.id.btn_equal);
        result = (TextView) findViewById(R.id.tv_result);
        dot = (Button) findViewById(R.id.btn_dot);
        C = (Button) findViewById(R.id.btn_C);
        modulo = (Button) findViewById(R.id.btn_modulo);
        onebyx = (Button) findViewById(R.id.btn_onebyx);
        xsquare=(Button) findViewById(R.id.btn_xsquare);


        one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                result.setText(result.getText() + "1");
            }
        });
        two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                result.setText(result.getText() + "2");
            }
        });
        three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                result.setText(result.getText()+"3");

            }
        });
        four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                result.setText(result.getText()+"4");

            }
        });
        five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                result.setText(result.getText()+"5");

            }
        });
        six.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                result.setText(result.getText()+"6");

            }
        });
        seven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                result.setText(result.getText()+"7");

            }
        });
        eight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                result.setText(result.getText()+"8");

            }
        });
        nine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                result.setText(result.getText()+"9");

            }
        });
        zero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                result.setText(result.getText()+"0");

            }
        });
        dot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                result.setText(result.getText()+".");

            }
        });

        //operation :

        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (result.getText() == "") {

                    result.setText("");
                } else {
                    val1 = Double.parseDouble(result.getText() +"");
                    add = true;

                    result.setText(null);
                }
            }
        });
        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (result.getText() == "") {

                    result.setText("");
                } else {
                    val1 = Double.parseDouble(result.getText() + "");
                    sub = true;
                    result.setText(null);
                }
            }
        });

        multiply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (result.getText() == "") {

                    result.setText("");
                } else {
                    val1 = Double.parseDouble(result.getText() + "");
                    mul = true;
                    result.setText(null);
                }
            }
        });
        divide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (result.getText() == "") {

                    result.setText("");
                } else {
                    val1 = Double.parseDouble(result.getText() + "");
                    div = true;
                    result.setText(null);
                }
            }
        });
        modulo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (result.getText() == "") {

                    result.setText("");
                } else {
                    val1 = Double.parseDouble(result.getText() + "");
                    mod = true;
                    result.setText(null);
                }
            }
        });
        onebyx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (result.getText() == "") {

                    result.setText("");
                } else {
                    val1 = 1;
                    obx = true;
                    result.setText(null);
                }
            }
        });
        xsquare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (result.getText() == "") {

                    result.setText("");
                } else {
                    val1 = Double.parseDouble(result.getText() + "");;
                    sqx = true;
                    result.setText(null);
                }
            }
        });

        equal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (result.getText() == "") {

                    result.setText("");
                } else {

                    equalcheck = true;
                    val2 = Double.parseDouble(result.getText() + "");
                    if (add == true) {


                        result.setText(val1 + val2+ "");
                        add = false;

                    }
                    if (sub == true) {
                        result.setText(val1 - val2 + "");
                        sub = false;
                    }
                    if (mul == true) {
                        result.setText(val1 * val2 + "");
                        mul = false;
                    }
                    if (div == true) {
                        result.setText(val1 / val2 + "");
                        div = false;
                    }
                    if (mod == true) {
                        result.setText(val1 % val2 + "");
                        mod = false;
                    }
                    if (obx == true) {
                        result.setText( val1 / val2 + "");
                        obx = false;
                    }
                    if (sqx == true) {
                        val2=val1;
                        result.setText( val1 * val2 + "");
                        sqx = false;
                    }

                }

            }

        });
        C.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                result.setText("");
            }
        });
    }
}



